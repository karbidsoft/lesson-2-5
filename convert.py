import glob
import os.path
import subprocess

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def read_dir_for_files(dir_name, pattern):
	files = glob.glob(os.path.join(BASE_DIR, os.path.join(dir_name, pattern)))
	return files


def convert_file_list(files, dest):
	for file in files:
		convert_process = subprocess.run(['sips', '--resampleWidth', '200', file, '--out', dest])
	print('Всего файлов сконвертированно: {}'.format(len(files)))


files = read_dir_for_files('Source', '*.*')
dest = os.path.join(BASE_DIR, 'Result')
os.makedirs(dest, mode=0o777, exist_ok=False)
convert_file_list(files, dest)
